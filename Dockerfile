FROM alpine:3.7
LABEL version="1.0.0"
LABEL maintainer="Michal Ochman <michal@kalambagames.com>"

RUN apk -v --update add \
        python \
        py-pip \
        git \
        git-lfs \
        openssh-client \
        zip \
        rsync \
        && \
    pip install --upgrade awscli && \
    apk -v --purge del py-pip && \
    rm /var/cache/apk/*
